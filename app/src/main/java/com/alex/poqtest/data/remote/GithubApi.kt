package com.alex.poqtest.data.remote

import com.alex.poqtest.data.model.Repo
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubApi {

    @GET("orgs/{organisation}/repos")
    suspend fun getOrganisationRepos(@Path("organisation") organisation: String): List<Repo>

}