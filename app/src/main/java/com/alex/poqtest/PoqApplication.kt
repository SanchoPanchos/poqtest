package com.alex.poqtest

import android.app.Application
import com.alex.poqtest.di.appModule
import com.alex.poqtest.di.repositoryModule
import com.alex.poqtest.di.viewModelModule
import org.koin.core.context.startKoin

class PoqApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(appModule, repositoryModule, viewModelModule)
        }
    }
}