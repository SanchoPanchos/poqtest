package com.alex.poqtest.ui.reposList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alex.poqtest.repository.ReposRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReposListViewModel(
    private val repository: ReposRepository,
    private val organisation: String
) : ViewModel() {

    private val _reposLiveData: MutableLiveData<List<RepoUiModel>> = MutableLiveData()
    val reposLiveData: LiveData<List<RepoUiModel>> = _reposLiveData

    private val _errorMessageLiveData = MutableLiveData<String?>()
    val errorMessageLiveData: LiveData<String?> = _errorMessageLiveData

    private val _dataLoadingLiveData = MutableLiveData<Boolean>()
    val dataLoadingLiveData: LiveData<Boolean> = _dataLoadingLiveData

    init {
        loadRepos()
    }

    private fun loadRepos() {
        viewModelScope.launch(Dispatchers.IO) {
            _dataLoadingLiveData.postValue(true)
            val result = repository.loadRepos(organisation)
            if (result.isSuccessful) {
                _reposLiveData.postValue(result.data?.map {
                    RepoUiModel(it.name ?: "", it.description ?: "")
                })
            } else {
                _errorMessageLiveData.postValue(result.error)
            }
            _dataLoadingLiveData.postValue(false)
        }
    }

}