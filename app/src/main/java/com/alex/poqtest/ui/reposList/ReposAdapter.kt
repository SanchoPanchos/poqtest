package com.alex.poqtest.ui.reposList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alex.poqtest.R
import kotlinx.android.synthetic.main.item_repo.view.*

class ReposAdapter : RecyclerView.Adapter<ReposAdapter.ReposViewHolder>() {

    private val reposList: MutableList<RepoUiModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReposViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return ReposViewHolder(view)
    }

    override fun getItemCount() = reposList.size

    override fun onBindViewHolder(holder: ReposViewHolder, position: Int) =
        holder.bind(reposList[position])

    fun append(newReposList: List<RepoUiModel>) {
        val oldSize = reposList.size
        reposList.addAll(newReposList)
        notifyItemRangeInserted(oldSize, reposList.size)
    }

    class ReposViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(repo: RepoUiModel) {
            with(itemView) {
                repoName.text = repo.name
                repoDescription.text = repo.description
            }
        }
    }
}