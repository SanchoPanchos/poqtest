package com.alex.poqtest.ui.reposList

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alex.poqtest.R
import com.alex.poqtest.base.BaseFragment
import com.alex.poqtest.util.showToast
import kotlinx.android.synthetic.main.fragment_repos.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class ReposListFragment : BaseFragment(R.layout.fragment_repos) {

    private val organisation = "square"
    private lateinit var adapter: ReposAdapter

    private val viewModel: ReposListViewModel by inject() {
        parametersOf(organisation)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        viewModel.reposLiveData.observe(viewLifecycleOwner, Observer {
            onReposLoaded(it)
        })
        viewModel.dataLoadingLiveData.observe(viewLifecycleOwner, Observer {
            showLoader(it)
        })
        viewModel.errorMessageLiveData.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })
    }

    private fun initRecycler() {
        adapter = ReposAdapter()
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter
    }

    private fun onReposLoaded(reposList: List<RepoUiModel>) {
        adapter.append(reposList)
    }

    private fun showLoader(showLoader: Boolean) {
        if (showLoader) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

}