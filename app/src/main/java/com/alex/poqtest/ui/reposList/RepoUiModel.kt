package com.alex.poqtest.ui.reposList

data class RepoUiModel(
    val name: String,
    val description: String
)