package com.alex.poqtest.base

import androidx.fragment.app.Fragment

abstract class BaseFragment(contentLayoutId : Int) : Fragment(contentLayoutId)