package com.alex.poqtest.repository

import com.alex.poqtest.base.Result
import com.alex.poqtest.data.model.Repo

interface ReposRepository {

    suspend fun loadRepos(organisation: String): Result<List<Repo>>

}