package com.alex.poqtest.repository

import com.alex.poqtest.base.Result
import com.alex.poqtest.util.safeApiCall
import com.alex.poqtest.data.model.Repo
import com.alex.poqtest.data.remote.GithubApi

class RemoteReposRepository(private val api: GithubApi) : ReposRepository {

    override suspend fun loadRepos(organisation: String): Result<List<Repo>> {
        return safeApiCall() {
            api.getOrganisationRepos(organisation)
        }
    }

}