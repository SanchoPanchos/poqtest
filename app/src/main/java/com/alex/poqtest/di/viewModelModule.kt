package com.alex.poqtest.di

import com.alex.poqtest.ui.reposList.ReposListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { (organisation: String) ->
        ReposListViewModel(get(), organisation)
    }

}