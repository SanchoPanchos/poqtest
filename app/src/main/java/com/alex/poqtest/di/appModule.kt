package com.alex.poqtest.di

import com.alex.poqtest.BuildConfig
import com.alex.poqtest.data.remote.GithubApi
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    single {
        provideRetrofit()
    }

    single {
        provideGithubApi(get())
    }

}

fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.API_URL)
        .client(OkHttpClient())
        .build()
}

fun provideGithubApi(retrofit: Retrofit): GithubApi {
    return retrofit.create(GithubApi::class.java)
}