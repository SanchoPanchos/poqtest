package com.alex.poqtest.di

import com.alex.poqtest.repository.RemoteReposRepository
import com.alex.poqtest.repository.ReposRepository
import org.koin.dsl.module

val repositoryModule = module {

    single {
        RemoteReposRepository(get()) as ReposRepository
    }

}
