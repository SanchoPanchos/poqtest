package com.alex.poqtest.util

import android.widget.Toast
import androidx.fragment.app.Fragment
import com.alex.poqtest.R
import com.alex.poqtest.base.Result

fun Fragment.showToast(message: String?) {
    if (message == null) {
        Toast.makeText(context, R.string.default_error, Toast.LENGTH_SHORT).show()
    } else {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}

suspend fun <T> safeApiCall(apiCall: suspend () -> T): Result<T> {
    return try {
        Result.success(apiCall.invoke())
    } catch (throwable: Throwable) {
        Result.error(throwable.message)
    }

}
