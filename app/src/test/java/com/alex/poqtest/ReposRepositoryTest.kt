package com.alex.poqtest

import com.alex.poqtest.base.Result
import com.alex.poqtest.data.model.Repo
import com.alex.poqtest.data.remote.GithubApi
import com.alex.poqtest.repository.RemoteReposRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.only
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ReposRepositoryTest {

    private lateinit var repository: RemoteReposRepository
    private lateinit var githubApi: GithubApi

    private val validOrganization = "Apple"

    private val reposList = listOf(Repo("", ""))
    private val successResult = Result.success(reposList)

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        githubApi = mock()
        repository = RemoteReposRepository(githubApi)
        runBlocking {
            whenever(githubApi.getOrganisationRepos(validOrganization)).thenReturn(reposList)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun testReposLoadedSuccess() {
        runBlocking {
            val result = repository.loadRepos(validOrganization)
            verify(githubApi, only()).getOrganisationRepos(validOrganization)
            assertEquals(result.data, successResult.data)
        }
    }

}